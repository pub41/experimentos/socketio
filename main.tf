terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.21.0"
    }
  }
}

provider "aws" {

  region  = var.aws_region
  profile = var.aws_profile
}

module "VPC" {
  source      = "../../global_modules/VPC"
  aws_profile = var.aws_profile
}


module "IAM" {
  source       = "../../global_modules/IAM"
  ms_user_name = "${var.prefix_user}${var.project_name}"
  common_tags  = local.common_tags
}

module "ECR" {
  source            = "../../global_modules/ECR"
  aws_ecr_repo_name = "${var.prefix_environment}${var.project_name}"
  common_tags = {
    Environment : "loadshark",
    ManagedBy : "terraform"
  }
}

module "ECS" {
  source                          = "../../global_modules/ECS"
  for_each                        = { for index, environment in var.environments : index => environment }
  aws_ecs_family_name             = "${var.prefix_environment}${var.project_name}"
  aws_task_container_port         = each.value.ecs_task_container_port
  docker_image                    = module.ECR.container_repository_address
  common_tags                     = local.common_tags
  cluster_name                    = var.ecs_cluster_name
  docker_tag                      = var.docker_tag
  loadbalancer_name               = data.aws_lb.default.name
  vpc_id                          = module.VPC.vpc_id
  loadbalancer_security_group_id  = module.SecurityGroup[0].id
  service_network_subnet_id       = module.VPC.subnets_lambda
  loadbalancer_http_listener_arn  = data.aws_lb_listener.http.arn
  loadbalancer_https_listener_arn = data.aws_lb_listener.https.arn
  task_execution_role_arn         = var.task_execution_role_arn
  aws_region                      = var.aws_region
  project_name                    = var.project_name
  path_health_check               = var.path_health_check
  path_pattern_lb                 = var.path_pattern_lb
  secret_manager_arn              = var.secret_manager.arn
  secret_manager_name             = var.secret_manager.name
  task_container_cpu              = var.task_container_cpu
  task_container_memory           = var.task_container_memory
  ecs_cluster_id                  = data.aws_ecs_cluster.default.arn
  enable_cloudwatch_logs          = var.enable_cloudwatch_logs
  assign_public_ip                = var.ecs_assign_public_ip
}

module "ECS_AutoScaling" {
  count                                 = var.enable_ECS_autoScaling ? 1 : 0
  source                                = "../../global_modules/ECS/AutoScaling"
  service_name                          = var.ecs_service_name
  cluster_name                          = var.ecs_cluster_name
  autoScaling_max_capacity              = var.autoScaling_max_capacity
  autoScaling_min_capacity              = var.autoScaling_min_capacity
  autoScaling_target_value_cpu          = var.autoScaling_target_value_cpu
  autoScaling_scale_out_cooldown_cpu    = var.autoScaling_scale_out_cooldown_cpu
  autoScaling_scale_in_cooldown_cpu     = var.autoScaling_scale_in_cooldown_cpu
  autoScaling_target_value_memory       = var.autoScaling_target_value_memory
  autoScaling_scale_out_cooldown_memory = var.autoScaling_scale_out_cooldown_memory
  autoScaling_scale_in_cooldown_memory  = var.autoScaling_scale_in_cooldown_memory
  depends_on = [
    module.ECS
  ]
}

module "SecurityGroup" {
  source       = "../../global_modules/SecurityGroup"
  service_name = var.ecs_service_name

  for_each       = { for index, environment in var.environments : index => environment }
  vpc_id         = module.VPC.vpc_id
  firewall_rules = var.firewall_rules
}
