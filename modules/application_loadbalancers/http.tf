resource "aws_alb_listener" "http_priv" {
  lifecycle {
    # NUNCA REMOVA ISSO / NEVER REMOVE THIS
    prevent_destroy = true
    ignore_changes  = all
  }

  load_balancer_arn = aws_lb.priv.arn
  port              = 80
  protocol          = "HTTP"
  tags              = {}
  tags_all          = {}

  default_action {
    order = 1
    type  = "fixed-response"

    fixed_response {
      content_type = "application/json"
      message_body = jsonencode(
        {
          message = "Hello loadbalance ${var.loadbalancer_name_priv}"
        }
      )
      status_code = "200"
    }
  }

  timeouts {}
}

resource "aws_alb_listener" "http_proxy" {
  lifecycle {
    # NUNCA REMOVA ISSO / NEVER REMOVE THIS
    prevent_destroy = true
    ignore_changes  = all
  }

  load_balancer_arn = aws_lb.proxy.arn
  port              = 80
  protocol          = "HTTP"
  tags              = {}
  tags_all          = {}

  default_action {
    order = 1
    type  = "fixed-response"

    fixed_response {
      content_type = "application/json"
      message_body = jsonencode(
        {
          message = "Hello loadbalance ${var.loadbalancer_name_proxy}"
        }
      )
      status_code = "200"
    }
  }

  timeouts {}
}