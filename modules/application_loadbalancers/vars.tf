variable "loadbalancer_name_priv" {
  type        = string
  description = "loadbalance name"
}
variable "loadbalancer_name_proxy" {
  type        = string
  description = "loadbalance name"
}

variable "domain_name" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "aws_region" {
  type = string
}
