
module "VPC" {
  source      = "../../global_modules/VPC"
  aws_profile = var.aws_profile
  aws_region  = var.aws_region
}
resource "aws_lb" "priv" {

  lifecycle {
    # NUNCA REMOVA ISSO / NEVER REMOVE THIS
    prevent_destroy = true
    ignore_changes  = all
  }

  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = false
  enable_http2                     = true
  idle_timeout                     = 300
  internal                         = true
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  name                             = var.loadbalancer_name_priv
  security_groups = [
    aws_security_group.priv.id
  ]

  subnets = module.VPC.subnets
  tags = {
    "Environment"     = "${terraform.workspace}"
    "LoadbalanceName" = "${var.loadbalancer_name_priv}"
  }

  tags_all = {
    "Environment"     = "${terraform.workspace}"
    "LoadbalanceName" = "${var.loadbalancer_name_priv}"
  }
  timeouts {}
}

resource "aws_lb" "proxy" {

  lifecycle {
    # NUNCA REMOVA ISSO / NEVER REMOVE THIS
    prevent_destroy = true
    ignore_changes  = all
  }

  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = false
  enable_http2                     = true
  idle_timeout                     = 300
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  name                             = var.loadbalancer_name_proxy
  security_groups = [
    aws_security_group.proxy.id
  ]

  subnets = module.VPC.subnets
  tags = {
    "Environment"     = "${terraform.workspace}"
    "LoadbalanceName" = "${var.loadbalancer_name_proxy}"
  }

  tags_all = {
    "Environment"     = "${terraform.workspace}"
    "LoadbalanceName" = "${var.loadbalancer_name_proxy}"
  }
  timeouts {}
}
