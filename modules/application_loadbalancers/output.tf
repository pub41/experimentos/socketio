# PRIV
output "loadbalancer_name_priv" {
  value = aws_lb.priv.name
}

output "http_arn_priv" {
  value = aws_alb_listener.http_priv.arn
}

output "https_arn_priv" {
  value = aws_alb_listener.https_priv.arn
}

output "aws_security_group_arn_priv" {
  value = aws_security_group.priv.arn
}

# PROXY

output "loadbalancer_name_proxy" {
  value = aws_lb.proxy.name
}

output "http_arn_proxy" {
  value = aws_alb_listener.http_proxy.arn
}

output "https_arn_proxy" {
  value = aws_alb_listener.https_proxy.arn
}

output "aws_security_group_arn_proxy" {
  value = aws_security_group.proxy.arn
}
