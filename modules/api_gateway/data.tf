data "aws_acm_certificate" "default" {
  domain   = "*.${var.domain_name}"
  statuses = ["ISSUED"]
}