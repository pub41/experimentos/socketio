resource "aws_apigatewayv2_api" "websocket" {
  name                       = var.apigateway_name
  protocol_type              = "WEBSOCKET"
  route_selection_expression = "$request.body.action"
  description                = "Websocket API Gateway"

}

module "aws_apigatewayv2_integration" {
  count = length(var.apigateway_recources)

  source             = "./modules/integration"
  credentials_arn    = aws_iam_role.this.arn
  api_id             = aws_apigatewayv2_api.websocket.id
  integration_type   = var.apigateway_recources[count.index].integration_type
  integration_uri    = (var.apigateway_recources[count.index].integration_type == "AWS_PROXY") ? "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.aws_region}:${var.aws_account_id}:function:${var.apigateway_recources[count.index].function_name}/invocations" : var.apigateway_recources[count.index].integration_uri
  integration_method = "POST"
  route              = var.apigateway_recources[count.index].route
}

module "route53-sub_domain" {
  source          = "../../global_modules/route53-sub_domain"
  target_address  = replace(aws_apigatewayv2_api.websocket.api_endpoint, "/wss:///", "")
  type            = "alias"
  sub_domain_name = var.apigateway_name
  target_domain_name = aws_apigatewayv2_domain_name.custom_domain.domain_name_configuration[0].target_domain_name
  target_domain_name_hosted_zone_id = aws_apigatewayv2_domain_name.custom_domain.domain_name_configuration[0].hosted_zone_id

}
