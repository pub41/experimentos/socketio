variable "api_id" {
  type = string
}

variable "integration_type" {
  type = string
}

variable "integration_uri" {
  type = string
}

variable "integration_method" {
  type = string
}

variable "credentials_arn" {
  type = string
}

variable "route" {
  type = string
}