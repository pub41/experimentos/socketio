resource "aws_apigatewayv2_integration" "this" {
  api_id                    = var.api_id
  integration_type          = var.integration_type
  integration_uri           = var.integration_uri
  integration_method        = var.integration_method
  credentials_arn           = var.credentials_arn
  content_handling_strategy = "CONVERT_TO_TEXT"
}

resource "aws_apigatewayv2_route" "target" {
  api_id     = var.api_id
  route_key  = var.route
  target     = "integrations/${aws_apigatewayv2_integration.this.id}"
  depends_on = [aws_apigatewayv2_integration.this]
}

resource "aws_apigatewayv2_integration_response" "this" {
  count                    = (length(regexall(".*$*", var.route)) > 0) ? 0 : 1
  api_id                   = var.api_id
  integration_id           = aws_apigatewayv2_integration.this.id
  integration_response_key = "/200/"
  depends_on               = [aws_apigatewayv2_integration.this, aws_apigatewayv2_route.target]
}