variable "aws_profile" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "apigateway_name" {
  type = string
}

variable "aws_account_id" {
  type = string
}

variable "lambdas" {
  type = list(any)
}

variable "apigateway_recources" {
  type = list(any)
}

variable "domain_name" {
  type = string
}