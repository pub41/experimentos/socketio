module "lambda" {

  count = length(var.lambdas)

  source                = "../../global_modules/lambda"
  aws_account_id        = var.aws_account_id
  function_name         = var.lambdas[count.index].function_name
  timeout_lambda        = 5
  output_path           = "${path.module}/lambda_assets/.output/${var.lambdas[count.index].function_name}.zip"
  function_dir          = "${path.module}/lambda_assets/functions/${var.lambdas[count.index].function_name}/"
  json_policy           = file("${path.module}/lambda_assets/policies/${var.lambdas[count.index].function_name}.json")
  environment_variables = var.lambdas[count.index].environment_variables_lambda
  handler               = "index.handler"
  runtime               = "nodejs18.x"
  aws_profile           = var.aws_profile
  role_arn              = "arn:aws:iam::${var.aws_account_id}:role/${var.lambdas[count.index].function_name}-role"

  depends_on = [module.role_policy]
}


module "role_policy" {
  count             = length(var.lambdas)
  source            = "../../global_modules/role_policy"
  name              = var.lambdas[count.index].role_lambda.role_name
  principal_service = var.lambdas[count.index].role_lambda.principal_service
  policy_actions    = var.lambdas[count.index].role_lambda.policy_actions
}
