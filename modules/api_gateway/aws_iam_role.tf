data "aws_iam_policy_document" "this" {
  statement {
    actions = [
      "lambda:InvokeFunction",
    ]
    effect    = "Allow"
    resources = ["arn:aws:lambda:${var.aws_region}:${var.aws_account_id}:function:*"]
  }
}

resource "aws_iam_policy" "this" {
  name   = "${var.apigateway_name}-ApiGatewayPaceyPermissionLambdaInvokePolicy"
  path   = "/"
  policy = data.aws_iam_policy_document.this.json
}

resource "aws_iam_role" "this" {
  name = "${var.apigateway_name}-ApiGatewayPaceyPermissionLambdaInvokeRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "apigateway.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = [aws_iam_policy.this.arn]
}