var https = require('https');

const api = process.env.api;
const apiToken = process.env.token;

exports.handler = async (event) => {
    const result = await new Promise((resolve, reject) => {
        const options = {
            host: api,
            path: '/ses/v1/websocket/disconnect',
            method: 'POST',
            headers: {
                'Authorization': 'JWT ' + apiToken,
                'connectionid': event.requestContext.connectionId
            }
        };
        
        const req = https.request(options, (res) => {
            let dataString = '';
            res.on('data', chunk => {
                dataString += chunk;
            });
            res.on('end', () => {
                resolve(dataString);
            });
        });

        req.on('error', (e) => {
          reject(e.message);
        });

        // send the request
        req.write('');
        req.end();
        
    });
    // TODO implement
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    return response;
};
