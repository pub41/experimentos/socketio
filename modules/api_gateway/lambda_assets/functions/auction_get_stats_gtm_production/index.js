var https = require('https');

const api = process.env.api;
let dataString = "";

exports.handler = async (event) => {
    dataString = "";
    const result = await new Promise((resolve, reject) => {
        let token = "";
        if(event.body) {
            let dataObj = JSON.parse(event.body);
            token = dataObj && dataObj.token ? dataObj.token : null;
        }
        const options = {
            host: api,
            path: '/auction/v1/lot/shippingcompany/countStatus',
            method: 'GET',
            headers: {
                'Authorization': 'JWT ' + token
            }
        };
        
        const req = https.request(options, (res) => {
            res.on('data', chunk => {
                dataString += chunk;
            });
            res.on('end', () => {
                resolve(dataString);
            });
        });

        req.on('error', (e) => {
          reject(e.message);
        });

        // send the request
        req.write('');
        req.end();
    });
    // TODO implement
    const response = {
        statusCode: 200,
        body: dataString
    };
    return response;
};