aws_region     = "us-east-1"
aws_profile    = "loadshark"
aws_account_id = "106646140177"
domain_name    = "loadshark.io"

apigateway_name = "production-websocket"
lambdas = [
  {
    function_name = "wss_connect_notification_production"
    environment_variables_lambda = {
      api   = "production-priv.loadshark.io"
      token = ""
    }
    role_lambda = {
      role_name         = "wss_connect_notification_production"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  },
  {
    function_name = "wss_disconnect_notification_production"
    environment_variables_lambda = {
      api   = "production-priv.loadshark.io"
      token = ""
    }
    role_lambda = {
      role_name         = "wss_disconnect_notification_production"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  },
  {
    function_name = "auction_get_stats_production"
    environment_variables_lambda = {
      api = "production-priv.loadshark.io"
    }
    role_lambda = {
      role_name         = "auction_get_stats_production"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  }
]

apigateway_recources = [
  {
    route            = "$default"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_connect_notification_production"
    function_name    = "wss_connect_notification_production"
  },
  {
    route            = "$connect"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_connect_notification_production"
    function_name    = "wss_connect_notification_production"
  },
  {
    route            = "$disconnect"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_disconnect_notification_production"
    function_name    = "wss_disconnect_notification_production"
  },
  {
    route            = "auctionstats"
    integration_type = "AWS_PROXY"
    integration_uri  = "auction_get_stats_production"
    function_name    = "auction_get_stats_production"
  },
  {
    route            = "ping"
    integration_type = "HTTP_PROXY"
    integration_uri  = "https://production-proxy.loadshark.io/ses/v1/ping"
  }
]
