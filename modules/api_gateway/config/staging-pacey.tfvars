aws_region     = "us-east-1"
aws_profile    = "pacey"
aws_account_id = "643956415805"
domain_name    = "pacey.com.br"

apigateway_name = "staging-websocket"
lambdas = [
  {
    function_name = "wss_connect_notification_gtm_staging"
    environment_variables_lambda = {
      api   = "staging-priv.pacey.com.br"
      token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXJ2aWNlIjoiU0VTIiwiZGF0ZSI6MTU5MTIyMjc1MDcwOCwiYXVkIjoiYXBpLmxvYWRzaGFyay5jb20iLCJpYXQiOjE1OTEyMjI3NTB9.xPwA1HWTxdHnRB4Iy0pDx6E7Y_MAjDr-b1FCU6cyzM8"
    }
    role_lambda = {
      role_name         = "wss_connect_notification_gtm_staging"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  },
  {
    function_name = "wss_disconnect_notification_gtm_staging"
    environment_variables_lambda = {
      api   = "staging-priv.pacey.com.br"
      token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXJ2aWNlIjoiU0VTIiwiZGF0ZSI6MTU5MTIyMjc1MDcwOCwiYXVkIjoiYXBpLmxvYWRzaGFyay5jb20iLCJpYXQiOjE1OTEyMjI3NTB9.xPwA1HWTxdHnRB4Iy0pDx6E7Y_MAjDr-b1FCU6cyzM8"
    }
    role_lambda = {
      role_name         = "wss_disconnect_notification_gtm_staging"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  },
  {
    function_name = "auction_get_stats_gtm_staging"
    environment_variables_lambda = {
      api = "staging-priv.pacey.com.br"
    }
    role_lambda = {
      role_name         = "auction_get_stats_gtm_staging"
      principal_service = "lambda.amazonaws.com"
      policy_actions = [
        "cloudwatch:GetMetricStatistics",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DescribeVpcs",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups"
      ]
    }
  }
]

apigateway_recources = [
  {
    route            = "$default"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_connect_notification_gtm_staging"
    function_name    = "wss_connect_notification_gtm_staging"
  },
  {
    route            = "$connect"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_connect_notification_gtm_staging"
    function_name    = "wss_connect_notification_gtm_staging"
  },
  {
    route            = "$disconnect"
    integration_type = "AWS_PROXY"
    integration_uri  = "wss_disconnect_notification_gtm_staging"
    function_name    = "wss_disconnect_notification_gtm_staging"
  },
  {
    route            = "auctionstats"
    integration_type = "AWS_PROXY"
    integration_uri  = "auction_get_stats_gtm_staging"
    function_name    = "auction_get_stats_gtm_staging"
  },
  {
    route            = "ping"
    integration_type = "HTTP_PROXY"
    integration_uri  = "https://staging-proxy.pacey.com.br/ses/v1/ping"
  }
]
