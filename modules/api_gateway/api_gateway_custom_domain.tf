resource "aws_apigatewayv2_domain_name" "custom_domain" {
  domain_name = "${var.apigateway_name}.${var.domain_name}"

  domain_name_configuration {
    certificate_arn = data.aws_acm_certificate.default.arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }
  depends_on = [ aws_apigatewayv2_api.websocket ]
}

resource "aws_apigatewayv2_api_mapping" "custom_domain" {
  api_id      = aws_apigatewayv2_api.websocket.id
  domain_name = aws_apigatewayv2_domain_name.custom_domain.id
  stage       = aws_apigatewayv2_stage.stage.id
}
