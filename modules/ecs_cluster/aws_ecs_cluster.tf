resource "aws_ecs_cluster" "default" {
  lifecycle {
    # NUNCA REMOVA ISSO / NEVER REMOVE THIS
    prevent_destroy = true
    ignore_changes  = all
  }

  # capacity_providers = [
  #     "FARGATE",
  #     "FARGATE_SPOT",
  # ]
  name = var.ecs_cluster_name
  tags = {
    "Environment" = "${terraform.workspace}x"
    "clusterName" = var.ecs_cluster_name
  }

  setting {
    name  = "containerInsights"
    value = (terraform.workspace == "development") ? "disabled" : "enabled"
  }
}