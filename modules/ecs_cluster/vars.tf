variable "ecs_cluster_name" {
  type = string
}
variable "aws_profile" {
  type = string
}

variable "aws_region" {
  type = string
}
