variable "aws_profile" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "firewall_rules" {
  type = list(object({
    description = string
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = string
  }))
}

variable "redis_name" {
  type = list(any)
}