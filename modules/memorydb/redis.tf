resource "aws_memorydb_cluster" "this" {

  count = length(var.redis_name)

  acl_name                   = "open-access"
  auto_minor_version_upgrade = true
  data_tiering               = false
  description                = terraform.workspace == "production" ? "Redis for production environment" : "General Redis for non-production environment"
  engine_version             = "7.0"
  maintenance_window         = "sun:03:30-sun:04:30"
  name                       = var.redis_name[count.index]
  node_type                  = "db.t4g.small"
  num_replicas_per_shard     = 0
  num_shards                 = 1
  parameter_group_name       = "default.memorydb-redis7"
  port                       = 6379
  security_group_ids         = [module.SecurityGroup[count.index].id]
  snapshot_retention_limit   = 0
  snapshot_window            = "05:30-06:30"
  subnet_group_name          = aws_memorydb_subnet_group.this[count.index].name
  tags                       = {}
  tags_all                   = {}
  tls_enabled                = false
  depends_on                 = [module.SecurityGroup]
}

data "aws_memorydb_cluster" "this" {
  count      = length(var.redis_name)
  name       = var.redis_name[count.index]
  depends_on = [aws_memorydb_cluster.this]
}

data "aws_route53_zone" "this" {
  name = (length(regexall(".*pacey.*", terraform.workspace)) > 0) ? "pacey.com.br." : "loadshark.io."
}

resource "aws_route53_record" "this" {

  count = length(data.aws_memorydb_cluster.this)

  zone_id    = data.aws_route53_zone.this.id
  name       = data.aws_memorydb_cluster.this[count.index].name
  type       = "CNAME"
  ttl        = 300
  records    = ["${data.aws_memorydb_cluster.this[count.index].name}.dqie55.clustercfg.memorydb.us-east-1.amazonaws.com"]
  depends_on = [aws_memorydb_cluster.this]
}


# module "subdomain_link" {
#     source          = "../../global_modules/route53-sub_domain"
#     aws_profile     = var.aws_profile
#     aws_region      = var.aws_region
#     sub_domain      = var.redis_name
# }