terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.21.0"
    }
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

module "VPC" {
  source      = "../../global_modules/VPC"
  aws_profile = var.aws_profile
}

module "SecurityGroup" {
  count          = length(var.redis_name)
  source         = "../../global_modules/SecurityGroup"
  service_name   = var.redis_name[count.index]
  vpc_id         = module.VPC.vpc_id
  firewall_rules = var.firewall_rules
}

resource "aws_memorydb_subnet_group" "this" {
  count      = length(var.redis_name)
  name       = var.redis_name[count.index]
  subnet_ids = module.VPC.subnets
}
