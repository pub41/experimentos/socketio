aws_region  = "us-east-1"
aws_profile = "pacey"

# rules firewall
firewall_rules = [
  {
    description = "HTTP"
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = "10.0.0.0/16"
  }
]

# list of the instances Redis for non-production environment
redis_name = ["sandbox-redis"]
