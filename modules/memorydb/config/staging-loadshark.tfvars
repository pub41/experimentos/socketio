aws_region  = "us-east-1"
aws_profile = "loadshark"

# rules firewall
firewall_rules = [
  {
    description = "HTTP"
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = "172.31.0.0/16"
  }
]
redis_name = ["sandbox-redis"]