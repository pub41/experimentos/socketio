
output "container_repository_address" {
  description = "The address to the image repository."
  value       = aws_ecr_repository.ecr_repo.repository_url
}
