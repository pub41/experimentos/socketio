
# terraform {
#   
#   required_providers {
#     awsenvironment = {
#       source  = "hashicorp/aws"
#       version = ">= 5.21.0"
#     }
#   }
# }

resource "aws_ecr_repository" "ecr_repo" {
  name = var.aws_ecr_repo_name
  tags = var.common_tags
  image_tag_mutability = "MUTABLE"
  force_delete = true
}
