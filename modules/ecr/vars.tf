
variable "aws_ecr_repo_name" {
  type        = string
  description = "The name of the ECR repository."
}

variable "common_tags" {
  type = object({
    Environment = string
    ManagedBy   = string
  })
  description = "The tags that generically describe elements from this Terraform script."
}
