variable "aws_region" {
  default = "us-east-1"
}

variable "bucket_name" {
  description = "name of the bucket that will use as origin for CDN"
  # default = "cdn-loadshark"
}

variable "retain_on_delete" {
  description = "Instruct CloudFront to simply disable the distribution instead of delete"
  default     = false
}

variable "price_class" {
  description = "Price classes provide you an option to lower the prices you pay to deliver content out of Amazon CloudFront"
  default     = "PriceClass_All"
}
variable "hosted_zone_id" {
  description = "The primary domain hosted zone ID - loadshark.io"
  #   default = "ZBMEH0ZNJSQNU"
}

variable "domain_name" {
  type        = string
  description = "Name of the domain where record(s) need to create"
}

# variable alias_zone_id {
# 	description = "This is always the hosted zone ID when you create an alias record that routes traffic to a CloudFront distribution."
# 	# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-route53-aliastarget.html
# 	# default = "Z2FDTNDATAQYW2"
#   type = string
# }

variable "acm_certificate_arn" {
  description = "Custom SSL certificate for domain"
  # default = "arn:aws:acm:us-east-1:106646140177:certificate/2769dfbc-a46b-4f2c-b212-787c9d260cc4"
}

variable "cloudfront_custom_error_responses" {
  type = list(any)
  default = [
    {
      error_code            = 403
      response_code         = 200
      error_caching_min_ttl = 0
      response_page_path    = "/index.html"
    },
    {
      error_code            = 404
      response_code         = 200
      error_caching_min_ttl = 0
      response_page_path    = "/index.html"
    }
  ]
}