variable "aws_ecs_family_name" {
  type        = string
  description = "The name of the ECS Task's family."
}

variable "aws_task_container_port" {
  type        = string
  description = "The port of the ECS Task's docker container."
  default     = 3000
}

variable "docker_image" {
  type        = string
  description = "The docker image address."
}

variable "docker_tag" {
  type        = string
  description = "The docker tag for the docker container image."
  default     = "latest"
}

variable "common_tags" {
  type = object({
    Environment = string
    ManagedBy   = string
  })
  description = "The tags that generically describe elements from this Terraform script."
}

variable "cluster_name" {
  type        = string
  description = "The cluster name."
}

variable "loadbalancer_name" {
  type        = string
  description = "The load_balancer name."
}

variable "vpc_id" {
  type        = string
  description = "The id of the AWS_VPC."
}

variable "loadbalancer_security_group_id" {
  type        = string
  description = "The id of the loadbalance security group."
}

variable "service_network_subnet_id" {
  type        = list(any)
  description = "The id of the subnet used by the ECS service."
}

variable "loadbalancer_http_listener_arn" {
  type        = string
  description = "The ARN of the HTTP ALB listener."
}

variable "loadbalancer_https_listener_arn" {
  type        = string
  description = "The ARN of the HTTPS ALB listener."
}

variable "project_name" {
  type = string
}

variable "task_execution_role_arn" {
  type        = string
  description = "The ARN of the task execution role."
}

variable "aws_region" {
  type = string
}

variable "path_health_check" {
  type = string
}

variable "path_pattern_lb" {
  type = string
}

variable "secret_manager_arn" {
  type    = string
  default = "arn:aws:secretsmanager:us-east-1:106646140177:secret:null-zPZrot"
}

variable "secret_manager_name" {
  type    = string
  default = "null_secret"
}

variable "task_container_cpu" {
  type = number
}

variable "task_container_memory" {
  type = number
}

variable "ecs_cluster_id" {
  type = string
}

variable "enable_cloudwatch_logs" {
  type    = bool
  default = true
}

variable "assign_public_ip" {
  type = bool
  default = true
}