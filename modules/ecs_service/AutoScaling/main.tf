
resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.autoScaling_max_capacity
  min_capacity       = var.autoScaling_min_capacity
  resource_id        = "service/${var.cluster_name}/${var.service_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

}

resource "aws_appautoscaling_policy" "ecs_target_cpu" {
  name               = "application-scaling-policy-cpu"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {

    target_value       = var.autoScaling_target_value_cpu
    scale_out_cooldown = var.autoScaling_scale_out_cooldown_cpu
    scale_in_cooldown  = var.autoScaling_scale_in_cooldown_cpu

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}

resource "aws_appautoscaling_policy" "ecs_target_memory" {
  name               = "application-scaling-policy-memory"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {

    target_value       = var.autoScaling_target_value_memory
    scale_out_cooldown = var.autoScaling_scale_out_cooldown_memory
    scale_in_cooldown  = var.autoScaling_scale_in_cooldown_memory

    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

  }
  depends_on = [aws_appautoscaling_target.ecs_target]
}