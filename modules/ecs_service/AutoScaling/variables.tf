variable "cluster_name" {
  type = string
}

variable "service_name" {
  type = string
}

variable "autoScaling_max_capacity" {
  type    = number
  default = 100
}

variable "autoScaling_min_capacity" {
  type    = number
  default = 1
}

variable "autoScaling_target_value_cpu" {
  type    = number
  default = 90
}

variable "autoScaling_scale_out_cooldown_cpu" {
  type    = number
  default = 60
}

variable "autoScaling_scale_in_cooldown_cpu" {
  type    = number
  default = 300
}

variable "autoScaling_target_value_memory" {
  type    = number
  default = 90
}

variable "autoScaling_scale_out_cooldown_memory" {
  type    = number
  default = 300
}

variable "autoScaling_scale_in_cooldown_memory" {
  type    = number
  default = 600
}