resource "aws_lb_target_group" "main" {
  name        = var.aws_ecs_family_name
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_id
  tags        = var.common_tags

  health_check {
    healthy_threshold   = "5"
    unhealthy_threshold = "5"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "7"
    path                = var.path_health_check
  }
}

resource "aws_lb_listener_rule" "http" {
  listener_arn = var.loadbalancer_http_listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }

  condition {
    path_pattern {
      values = ["${var.path_pattern_lb}*"]
    }
  }
}

resource "aws_lb_listener_rule" "https" {
  listener_arn = var.loadbalancer_https_listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }

  condition {
    path_pattern {
      values = ["${var.path_pattern_lb}*"]
    }
  }
}

resource "aws_cloudwatch_log_group" "ecs_task_main" {
  name              = "/ecs/${var.aws_ecs_family_name}"
  retention_in_days = 1
  tags              = var.common_tags
}

resource "aws_ecs_task_definition" "main" {
  family                   = var.aws_ecs_family_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.task_container_cpu
  memory                   = var.task_container_memory
  tags                     = var.common_tags
  execution_role_arn       = var.task_execution_role_arn
  task_role_arn            = var.task_execution_role_arn
  container_definitions = templatefile(
    (var.enable_cloudwatch_logs == true) ? "${path.module}/templates/container_definition.tftpl" : "${path.module}/templates/container_definition_disableCloudwatchLogs.tftpl",
    {
      "container_name"           = var.aws_ecs_family_name,
      "container_port"           = var.aws_task_container_port,
      "docker_image"             = "${var.docker_image}:${var.docker_tag}",
      "service_name"             = var.aws_ecs_family_name,
      "aws_region"               = var.aws_region
      "aws_cloudwatch_log_group" = "/ecs/${var.aws_ecs_family_name}"
      "secret_manager_arn"       = var.secret_manager_arn
      "secret_manager_name"      = var.secret_manager_name
    }
  )
  depends_on = [
    aws_cloudwatch_log_group.ecs_task_main
  ]
}

resource "aws_ecs_service" "main" {
  name            = var.aws_ecs_family_name
  cluster         = var.ecs_cluster_id
  task_definition = aws_ecs_task_definition.main.arn
  launch_type     = "FARGATE"
  desired_count   = 1
  tags            = var.common_tags

  network_configuration {
    subnets          = var.service_network_subnet_id
    security_groups  = [var.loadbalancer_security_group_id]
    assign_public_ip = var.assign_public_ip
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.main.id
    container_name   = var.aws_ecs_family_name
    container_port   = var.aws_task_container_port
  }

  depends_on = [aws_ecs_task_definition.main]
}
