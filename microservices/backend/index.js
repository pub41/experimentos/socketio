const express = require('express')
const { createServer } = require('node:http')
const app = express()
const serverApp = createServer(app)
const { authAsSocket } = require('./src/authenticate')
require('dotenv').config({ path: './.env' })
var cors = require('cors')
const io = require("socket.io")(serverApp, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
})
app.use(cors())
require('./router')(app)
io.use(authAsSocket)
require('./src/socketOrchestration')(io)

serverApp.listen(PORT = 9000, () => {
  console.log(`server running at http://localhost:${PORT}`)
})
