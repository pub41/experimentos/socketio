const single = (io, room, socketId, message) => {
  console.log('single:', room, socketId, message)
  io.to(socketId).emit(room, message);
}

const broadcast = (io, room, message) => {
  console.log('Broadcast:', room, message)
  io.emit(room, message);
}
 
module.exports = {
  single,
  broadcast
}
