const { single, broadcast } = require('../src/engineBackend')
const redis = require('./redis')

const rooms = process.env.ROOMS.split(",")

module.exports = (io) => {
    io.on('connection', (socket) => {
    
        /* 
          redister in Redis
        */
        redis.set(socket.id)
    
        /*
          message orchestration
        */
        for (i of rooms) {
        
          socket.on(i, (data) => {
            
            if ( data['broadcast'] ) {
              broadcast(io, data['room'], data['message'])
            } else {
              single(io, data['room'], data['socketId'], data['message'])
            }
    
        })
      }
    
      /* 
        disconnect and remove from Redis
      */
      socket.on("disconnect", (reason) => {
        redis.delete(socket.id)
      });  
    
    })
}
