const { authAsMiddleware } = require('../src/authenticate')
const { healthCheckValidations } = require('./health-check')

module.exports = (app) => {

    /*
        Health-Check
    */
    app.get('/websocket/health-check',async (req, res) => {

        let validations = await healthCheckValidations()
        if (validations.status) {
            res.status(200).json("It's okay!")
            return
        }
        res.status(503).json(validations.message)
    })    

    /*
        endpoint to delivery rooms before authenticate middleware.
    */
    app.get('/getAllRooms', authAsMiddleware, (req, res) => {
    
        res.json(process.env.ROOMS.split(","))
    
    })

}