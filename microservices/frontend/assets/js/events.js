form.addEventListener('submit', (e) => {
    e.preventDefault();
        socket.emit(document.getElementById('room').value, {
            room: document.getElementById('room').value,
            socketId: document.getElementById('destination').value,
            broadcast: document.getElementById('broadcast').checked,
            // recipient: socketId,
            message: document.getElementById('message').value  +" - "+ socketId
        });
        document.getElementById('message').value = ''
});

document.getElementById('room').addEventListener('change', (data) => {
    setRoomChannel(data.target.value)
    console.log(data.target.value)
})    