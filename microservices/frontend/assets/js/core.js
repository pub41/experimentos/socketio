var socketId = null
var socket = null
var connected = false
var AUTHORIZATION_TOKEN = "f067fec1433f99e3dc0eabb0f152ef04"

const isConnect = (status, message) => {
    
    let elemStatus = document.getElementById('status')

    if (status) {
        elemStatus.innerHTML = message
        elemStatus.classList.remove('disconnected')        
    } else {
        elemStatus.innerHTML = message
        elemStatus.classList.toggle('disconnected')
    }
}

const http = axios.create({
    'Content-Type': 'application/json',
    authorization: AUTHORIZATION_TOKEN
})

const setRoomChannel = (room) => {
    // open the channel in the Websocket
    socket.on(room, (msg) => {
        const item = document.createElement('li')
        item.classList.add("list-group-item", "list-group-item-light")
        item.textContent = msg
        messages.appendChild(item)
        window.scrollTo(0, document.body.scrollHeight);
    });
}

try {
    socket = io('http://localhost:9000', {
        auth: {
            token: AUTHORIZATION_TOKEN
        }
    })
    connected = true
} catch (e) {
    isConnect(false, `Check your connection (${e.message})`)
    throw new Error(e.message);
}

if (connected) {
    socket.on('connect', () => {
        socketId = socket.id
        document.getElementById("mySocketId").value = socketId
        isConnect(true, 'You is connected!')
    })
    
    socket.on("connect_error", (err) => {
        isConnect(false, `Check your connection (${err.message})`)
    });
    
    socket.on("disconnect", () => {
        isConnect(false, 'Check you authentication.')
    });
      
    axios.get("http://localhost:9000/getAllRooms",
        {
            headers: {"authorization": AUTHORIZATION_TOKEN},
            withCredentials: false
        }
    ).then( response => {
            
            for (room of response.data) {
                document.getElementById('room').innerHTML += `<option value=${room}> ${room} </option>`
            }
        })
}


