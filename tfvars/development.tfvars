# AWS Credentials & Access config
aws_region              = "us-east-1"
aws_account_id          = "643956415805"
aws_profile             = "pacey"

# General config
prefix_environment = "tf-development-"
project_name       = "websocket"

# cloudWatch
cloudwatch_reference_name = [
  {
    branches = ["master"]
    prefixes = []
  }
]

cloudwatch_reference_type = ["branch"]
enable_cloudwatch_logs    = false

#ECS
task_execution_role_arn = "arn:aws:iam::643956415805:role/ecsTaskExecutionRole"
docker_tag              = "development"
secret_manager = {
  arn  = "arn:aws:secretsmanager:us-east-1:643956415805:secret:development-websocket-x"
  name = "AUTH_SERVICE_SECRETS"
}
ecs_cluster_name                      = "tf-development"
ecs_service_name                      = "tf-development-websocket"
task_container_cpu                    = 256
task_container_memory                 = 512
autoScaling_max_capacity              = 2
autoScaling_min_capacity              = 1
autoScaling_target_value_cpu          = 60
autoScaling_scale_out_cooldown_cpu    = 20
autoScaling_scale_in_cooldown_cpu     = 300
autoScaling_target_value_memory       = 80
autoScaling_scale_out_cooldown_memory = 20
autoScaling_scale_in_cooldown_memory  = 600
enable_ECS_autoScaling                = true

# Target Group - health check
path_health_check = "/websocket/v1/health-check"
path_pattern_lb   = "/websocket"

# Environments config
environments = [
  {
    sns_sqs_resource_name_list = [
      "tf-development-websocket.fifo",
      "tf-development-websocket-dead-letter.fifo",
      "tf-development-websocket-websocket.fifo",
    ],
    ecs_task_container_port = 80
  }
]

# Codebuild
codebuild_environment_variable = {
  ACCOUNT_ID               = "643956415805"
  ENVIRONMENT              = "developmentGtm"
  ECR_REPOSITORY_NAME      = "tf-development-websocket"
  ECR_REPOSITORY_IMAGE_TAG = "development"
}

# rules firewall
firewall_rules = [
  {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "10.0.0.0/16"
  }
]

loadbalancer_name = "pacey-development-priv"
containerInsights = "disabled"
domain_name       = "pacey.com.br"